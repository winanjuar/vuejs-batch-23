//soal 1
const hitungLuas = (panjang, lebar) => {
  return panjang * lebar;
};

const hitungKeliling = (panjang, lebar) => {
  return 2 * (panjang + lebar);
};

const p = 10;
const l = 8;
console.log(hitungLuas(p, l));
console.log(hitungKeliling(p, l));

// soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

newFunction("Sugeng", "Winanjuar").fullName();

// soal 3
const newObject = {
  firstName: "Sugeng",
  lastName: "Winanjuar",
  address: "Cirebon",
  hobby: "Listening to Music",
};

const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
console.log(combined);

//soal 5
const planet = "earth";
const view = "glass";

const result = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`;
console.log(result);
