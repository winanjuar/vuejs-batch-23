//soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

//jawaban 1
var arrPertama = pertama.split(" ");
var arrKedua = kedua.split(" ");
var answer1 = [
  arrPertama[0],
  arrPertama[2],
  arrKedua[0],
  arrKedua[1].toUpperCase(),
];
console.log(answer1.join(" "));

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

//jawaban 2
var answer2 =
  (parseInt(kataPertama) - parseInt(kataKedua) - parseInt(kataKetiga)) *
  parseInt(kataKeempat);
console.log(answer2);

//soal 3
var kalimat = "wah javascript itu keren sekali";
var kataPertama = kalimat.substring(0, 3);

//jawaban 3
var arrKalimat = kalimat.split(" ");
var kataKedua = arrKalimat[1];
var kataKetiga = arrKalimat[2];
var kataKeempat = arrKalimat[3];
var kataKelima = arrKalimat[4];

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
