//soal 1
var daftarBuah = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

//jawaban
daftarBuah.sort();
for (var i = 0; i < daftarBuah.length; i++) {
  console.log(daftarBuah[i]);
}

//soal 2
var data = {
  name: "Sugeng",
  age: 32,
  address: "Cirebon, Jabar",
  hobby: "Hiking",
};

//jawaban 2
function introduce(data) {
  return (
    "Nama saya " +
    data.name +
    ", umur saya " +
    data.age +
    " tahun, alamat saya di " +
    data.address +
    ", dan saya punya hobby yaitu " +
    data.hobby
  );
}

var perkenalan = introduce(data);
console.log(perkenalan);

//jawaban 3
function hitung_huruf_vokal(nama) {
  var vokal = ["a", "i", "u", "e", "o"];
  var result = 0;
  for (var i = 0; i < nama.length; i++) {
    if (vokal.includes(nama[i].toLowerCase())) result++;
  }
  return result;
}

var hitung_1 = hitung_huruf_vokal("Sugeng");
var hitung_2 = hitung_huruf_vokal("Winanjuar");
console.log(hitung_1, hitung_2);

//jawaban 4
function hitung(nilai) {
  return nilai * 2 - 2;
}
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
