//soal 2

var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

const waktuAwal = 9000;

readBooksPromise(waktuAwal, books[0])
  .then((sisaWaktu) => readBooksPromise(sisaWaktu, books[1]))
  .then((sisaWaktu) => readBooksPromise(sisaWaktu, books[2]))
  .catch((sisaWaktu) =>
    console.log(`Sisa waktu yang ada kurang ${Math.abs(sisaWaktu)}`)
  );
