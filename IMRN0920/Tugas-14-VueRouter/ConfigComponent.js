export const ConfigComponent = {
  data() {
    return {
      configs: [
        {
          id: 1,
          name: "Set Volume Audio",
        },
        {
          id: 2,
          name: "Set Quality Video",
        },
      ],
    };
  },
  template: `
      <div>
          <h1>Setting</h1>
          <ul>
              <li v-for="config of configs">
                  {{ config.name }} 
              </li>
          </ul>
      </div>
  `,
};
