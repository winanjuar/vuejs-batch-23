//soal 1
var nilai = 75;

//jawaban 1
if (nilai >= 85) console.log("indexnya A");
else if (nilai >= 75) console.log("indexnya B");
else if (nilai >= 65) console.log("indexnya C");
else if (nilai >= 55) console.log("indexnya D");
else console.log("indexnya E");

//soal 2
var tanggal = 7;
var bulan = 4;
var tahun = 1988;

//jawaban 2
var strBulan = "";
switch (bulan) {
  case 1:
    strBulan = "Januari";
    break;
  case 2:
    strBulan = "Februari";
    break;
  case 3:
    strBulan = "Maret";
    break;
  case 4:
    strBulan = "April";
    break;
  case 5:
    strBulan = "Mei";
    break;
  case 6:
    strBulan = "Juni";
    break;
  case 7:
    strBulan = "Juli";
    break;
  case 8:
    strBulan = "Agustus";
    break;
  case 9:
    strBulan = "September";
    break;
  case 10:
    strBulan = "Oktober";
    break;
  case 11:
    strBulan = "November";
    break;
  case 12:
    strBulan = "Desember";
    break;
  default:
    strBulan = "";
    break;
}
console.log(tanggal + " " + strBulan + " " + tahun);

//soal 3
var n = 7;

//jawaban 3
for (var i = 1; i <= n; i++) {
  var kres = "";
  for (var j = 1; j <= i; j++) kres += "#";
  console.log(kres);
}

//soal 4
var m = 10;

//jawaban 4
for (var i = 1; i <= m; i++) {
  if (i % 3 == 1) console.log(i + " - I love programming");
  if (i % 3 == 2) console.log(i + " - I love Javascript");
  if (i % 3 == 0) {
    console.log(i + " - I love VueJS");
    var samadengan = "";
    for (var j = 1; j <= i; j++) samadengan += "=";
    console.log(samadengan);
  }
}
