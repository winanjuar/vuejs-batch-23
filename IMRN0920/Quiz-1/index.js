// Soal 1
function jumlah_kata(kalimat) {
  var arrKata = kalimat.trim().split(" ");
  return arrKata.length;
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";
console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));

// Soal 2
function next_date(tanggal, bulan, tahun) {
  var tomorrow = new Date(tahun, bulan - 1, tanggal);
  tomorrow.setDate(tomorrow.getDate() + 1);

  var theDate = tomorrow.getDate();
  var theMonth = tomorrow.getMonth() + 1;
  switch (theMonth) {
    case 1:
      strBulan = "Januari";
      break;
    case 2:
      strBulan = "Februari";
      break;
    case 3:
      strBulan = "Maret";
      break;
    case 4:
      strBulan = "April";
      break;
    case 5:
      strBulan = "Mei";
      break;
    case 6:
      strBulan = "Juni";
      break;
    case 7:
      strBulan = "Juli";
      break;
    case 8:
      strBulan = "Agustus";
      break;
    case 9:
      strBulan = "September";
      break;
    case 10:
      strBulan = "Oktober";
      break;
    case 11:
      strBulan = "November";
      break;
    case 12:
      strBulan = "Desember";
      break;
    default:
      strBulan = "";
      break;
  }
  var theYear = tomorrow.getFullYear();
  return theDate + " " + strBulan + " " + theYear;
}

var tanggal = 31;
var bulan = 12;
var tahun = 2020;
console.log(next_date(tanggal, bulan, tahun));
