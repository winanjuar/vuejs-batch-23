export const TodoComponent = {
  data() {
    return {
      todos: [
        {
          id: 1,
          name: "Coding",
        },
        {
          id: 2,
          name: "Singing",
        },
        {
          id: 3,
          name: "Working",
        },
      ],
    };
  },
  template: `
      <div>
          <h1>List Todo</h1>
          <ul>
              <li v-for="todo of todos">
                  {{ todo.name }} 
              </li>
          </ul>
      </div>
  `,
};
